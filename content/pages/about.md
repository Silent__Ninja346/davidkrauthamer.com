+++
title = "About Me"
path = "about"
+++

Hello there adventurer, you seem to have found my website!

Here's a bit about me if you're interested.

## Employment

Currently, I'm a Computer Engineer at [Circle Optics](https://circleoptics.com),
specializing in GPU programming with CUDA. I'm working on accelerating image
processing algorithms for the [Hydra II](https://circleoptics.com/hydra-ii/)
system. Additionally, I'm developing the foundation for our internal developer
tools using ImGui and SDL2. I also maintain a centralized storage server to
ensure our remote team has access to footage and datasets.

## Education

I graduated as a 5/5 Electrical Engineer at Stevens Institute of Technology in
May of 2024. While there I completed two co-op terms, multiple Summers of
research (including a first place prize for my work on
[XBF](https://github.com/XtensibleBinaryFormat/XBF)), and was one of four
students to receive the Bachelor Award In Electrical Engineering in the class
of 2024.

## More Places to Find Me

- [GitLab](https://gitlab.com/dkrautha)
  - Where my personal projects live.
- [GitHub](https://github.com/dkrautha)
  - Where my former classwork and Summer research lives.
  - I also use it for work, but I can't show you that :(
- [LinkedIn](https://www.linkedin.com/in/dkrautha)
  - Here's hoping I look vaguely professional...

## Interests

### Homelabs

Here are the specs for my current setup:

- 4 x 4 TB hard drives in a RAIDZ1
- 2 x 2 TB mirrored metadata drives
- 64 GB DDR4 RAM
- Ryzen 7 5825u
- All in
  [this](https://aoostar.com/products/aoostar-wtr-pro-4-bay-90t-storage-amd-ryzen-7-5825u-nas-mini-pc-support-2-5-3-5-hdd-复制)
  lovely case
  
The current services I'm running:

- Tailscale: Access without port forwarding.
- cloudflared: Expose just a few things to my domain.
- drawio: Quick random diagrams, exposed to the internet.
- filebrowser: Simple but effective Google Drive-like UI.
- foundryvtt: Online VTT.
- Jellyfin: Music library streaming.
- Kavita: Reading my ebook library.
- PhotoPrism: Tagging and categorizing my meme collection. 
- Syncthing: Painless syncthing between devices.

### Video Games

Here are some of the more recent titles I've been playing:

- Baldur's Gate 3
- Persona 4 Golden
- Balatro
- OneShot: World Machine Edition
- Ratchet & Clank: Rift Apart
- Devil May Cry 5

### Tabletop Role Playing Games

One of the hobbies I picked up since I starting at Stevens was TTRPGs. Like many
before me I started with Dungeon's and Dragons 5e, but after the whole OGL
fiasco our group tried out Pathfinder 2nd edition and we've loved it. I've also
tried Cyberpunk RED, and Lancer (Titanfall 2 but it's a TTRPG), and both are fantastic!

### Books

- The Inheritance Cycle
- The Ender Quintet
- The Hitchhiker's Guide to the Galaxy
- Altered Carbon
- The Fault in Our Stars