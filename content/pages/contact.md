+++
title = "Contact Me"
path = "contact"
+++

If you'd like to contact me the best way to do so is at the following email
address: [dkrautha@pm.me](mailto:dkrautha@pm.me).