+++
title = "Generating compile_commands.json"
date = "2021-07-13"
[taxonomies]
tags = ["c", "cpp", "build system"]
+++

# What the heck is this?

When I first started doing C++ programming this Summer with Grail, I wanted to
use Emacs as my primary editor. I quickly found out that
[`ccls`](https://github.com/MaskRay/ccls) exists, and was well integrated into
Doom Emacs. The primary issue with this setup was that `ccls` couldn't tell
where different imports were, and kept giving an error. The way that Clang based
language servers (like `clangd`) work is they require a `compile_commands.json`
file which contains the commands that run on each file in the project when
compiled. Without one of these, the language server doesn't know how to index
the project, and starts throwing out false errors.

## How do I get one?

### The easy way

<!-- TODO: explain how to do this with meson as well / where to find it -->

If your project uses CMake, set the variable `CMAKE_EXPORT_COMPILE_COMMANDS` to
`ON` and a `compile_commands.json` will generate in the build folder. Symlink
that thing to the root directory of the project and you're good to go.

If you like to have your sanity while using a build system and use Meson, the
JSON file should be automatically generated for you and placed in your build
files folder.

### The slightly more painful way

If your project doesn't use CMake (Grail used a bizarre and confusing system
using make before this Summer), the best way to generate one of these files is
going to be with [bear](https://github.com/rizsotto/Bear). As per the README,
generating the file is as easy as:

```bash
bear -- <your-build-command>
```
