+++
title = "Debian Backup Script"
date = "2021-07-10"
[taxonomies]
tags = ["debian", "ubuntu", "script", "bash"]
+++

# Author's Note 02/13/22

This note is out of date, as I'm no longer using a Debian based distribution on
my laptop. However, for anybody using Debian/Ubuntu based distributions and
dealing with the pain of multiple package managers, hopefully this guide can
still be of use.

# Preface

I've been using Pop!\_OS for ages now, and I only ended up doing a clean
reinstall once because I wanted to try out the beta build of 21.04. Despite this
amazing track record, I still try and prepare like my installation is going to
instantaneously explode and any moment, and I'll have to reinstall everything
from scratch. Here's what I do to prepare.

# Backing up manually installed apt packages

```bash
apt-mark showmanual > ~/Documents/new_machine/manual_installed
echo "Successfully backed up manually installed packages"
```

It's worth noting that often times this list will contain more than just the
packages you've manually installed, and usually will have packages installed by
default in the OS, but it won't include the massive list of dependencies.

# Backing up programs installed with flatpak

```bash
flatpak list --columns=application > ~/Documents/new_machine/flatpaks
echo "Successfully backed up installed flatpaks"
```

# Backing up dconf data

```bash
dconf dump / > ~/Documents/new_machine/xenon_backup
echo "Successfully backed up dconf configuration"
```

# Backing up pipx packages

```bash
pipx list | grep package | awk '{print $2}' | grep -v pipx > ~/Documents/new_machine/pipx_backup
echo "Successfully backed up pipx programs"
```

This is one of the more interesting commands, here's how it works.

## 1. `pipx list`

    venvs are in /home/david/.local/pipx/venvs
    apps are exposed on your $PATH at /home/david/.local/bin package
    gdtoolkit 3.2.7, Python 3.9.4
      - gdformat
      - gdlint
      - gdparse

The only thing I want from this output is the name after the word package,
`gdtoolkit`, so I need to cut out everything else besides that.

## 2. `grep package`

    package gdtoolkit 3.2.7, Python 3.9.4

If you know what grep is, then this is pretty self-explanatory. If not, grep
searches either a file or whatever is given to it from stdin for the supplied
regex (in this case just a word) and sends to stdout the lines of text
containing the regex.

## 3. `awk {print $2}`

    gdtoolkit

Awk is a bit of a magical tool that I don't fully understand, but what you see
here just gets the second word from the previous output, which in this case is
the name of the package.

# Backup fisher plugins

```bash
fish -c 'fisher list' > ~/Documents/new_machine/fisher_backup
echo "Successfully backed up fisher plugins"
```

# Full system backup

For backing up the state of the system (everything in the root directory besides
home folders more or less), I have a program called
[timeshift](https://github.com/teejee2008/timeshift) making daily backups.

# Home folder backup

For backing up my home folder I use
[Deja Dup](https://wiki.gnome.org/Apps/DejaDup), and have it backup via SFTP to
the NAS on my home network.
