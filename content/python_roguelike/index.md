+++
date = "2021-05-30"
title = "Caverns of Wintermoor"
[taxonomies]
tags = ["python", "roguelike", "game development"]
+++

# General Info

With my somewhat recent introduction to roguelike games, and needing to make a
final project for EE551: Engineering Python at Stevens, I figured I'd try my
hand at making a roguelike myself. Seeing as the class is teaching Python, I
went with the best documented roguelike engine I could find,
[libtcod](https://github.com/libtcod/libtcod), which conveniently has amazing
[Python bindings](https://github.com/libtcod/python-tcod). From the
[r/roguelikedev](https://www.reddit.com/r/roguelikedev/) subreddit I found a
very comprehensive [tutorial](http://rogueliketutorials.com/tutorials/tcod/v2/)
detailing how to get off the ground and running with libtcod, which this project
was largely based off of and expanded on.

The source code for this project can be found here:
<https://gitlab.com/dkrautha/python-roguelike>

# Current State

As of the latest update to this post, this project is inactive. I briefly tried
porting what was here over to Rust, but I've yet to find the time or motivation
to finish it, or improve this project further.

# Screenshots

## Title Screen

![Title Screen](./title_screen.webp)

## Combat

![Combat](./combat.webp)

## Random Maps

![Random Maps](./fully_explored_random_dungeon.webp)

## Message History

![Message History](./message_history.webp)

## Finding Items

![Finding Items](./finding_an_item.webp)

## Descending

![Descending](./staircase.webp)

## Character Info

![Character Info](./character_info.webp)
