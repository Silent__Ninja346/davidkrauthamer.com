+++
title = "My missing NixOS Arc"
date = "2024-12-10"
[taxonomies]
tags = ["nix", "nixos", "linux"]
+++

It's been... a long time since I've written anything here. I've gone through the gauntlet of using and loving NixOS, to now using a MacBook. In time I'll be writing a bit here to talk about my experience with NixOS, why you should, or shouldn't use it, and why I'm now a firm believer that the command key is awesome. 

Stay tuned :\)