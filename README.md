# davidkrauthamer.com

## Technologies

- [Zola](https://www.getzola.org/documentation/getting-started/overview/): Static Site Generation
- [Terminimal Theme](https://github.com/pawroman/zola-theme-terminimal): Chosen Hugo Theme

## Setup

1. Install zola however you want.
2. Clone this repository: `git clone --recurse-submodules <repo_url>`
3. Run the following: `zola serve`
